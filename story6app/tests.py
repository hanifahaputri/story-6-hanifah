from django.test import TestCase, Client
from story6app.views import index
from .models import NewStatus
from .forms import Form

class Story6UnitTest(TestCase):
    def test_story6_url_exists(self):
        url_response = Client().get('/')
        self.assertEqual(url_response.status_code, 200)

    def test_story6_helloworld_template(self):
        url_response = Client().get('/')
        self.assertTemplateUsed(url_response, "home.html")

    def test_story6_landingpage_helloworld(self):
        url_response = Client().get('/')
        html_response = url_response.content.decode('utf8')
        self.assertIn('Hello World!', html_response)

    def test_model_new_status_can(self):
        activity = NewStatus.objects.create(title="PPW Story 6 Can Receive Form")
        count_avail_status = NewStatus.objects.all().count()
        self.assertEqual(count_avail_status, 1)

    def test_story6_success_render(self):
        test = "Anonymous"
        response_test = Client().post('/', {'title': test})
        self.assertEqual(response_test.status_code, 302)

        response = Client().get('/')
        response_html = response.content.decode('UTF8')
        self.assertIn(test, response_html)

    def test_story6_error_render(self):
        test = "Anonymous"
        response_test = Client().post('/')
        self.assertEqual(response_test.status_code, 200)

        response = Client().get('/')
        response_html = response.content.decode('UTF8')
        self.assertNotIn(test, response_html)