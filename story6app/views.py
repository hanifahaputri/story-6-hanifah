from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form
from .models import NewStatus

def index(request):
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			data_item = form.save(commit=False)
			data_item.save()
			return HttpResponseRedirect('/')
	form = Form()
	status = NewStatus.objects.all()
	return render(request, 'home.html', {'form':form, 'status':status})
